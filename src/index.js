const express = require("express");
const app = express();

const port = 9000 || process.env["PORT"];

app.use(express.static(`${__dirname}/../public`));
app.use(require("./images/routes"));

app.listen(port, () => {
  console.log(`Service started listening on ${port} port.`);
});
