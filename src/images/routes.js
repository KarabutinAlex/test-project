const { Router } = require("express");

const app = new Router();

const takeEvery = async (list, chunkSize, callback) => {
  let offset = 0;
  while (offset < list.length) {
    const chunk = list.slice(offset, chunkSize);
    await callback(chunk);
    offset += chunk.length;
  }
};

const imageSizes = [
  { width: 100, height: 100 },
  { width: 120, height: 120 },
  // ...
];

const createImage = (width, height) => ({ path: `${width}x${height}.jpg` });
const uploadImage = (path) => Promise.resolve();

app.post(
  "/generate",
  (req, res) => {
    res
      .status(202)
      .json({ message: "Image generating started successfully." });

    takeEvery(imageSizes, 100, async (chunk) => {
      const images = await Promise.all(
        chunk.map(size => createImage(size.width, size.height))
      );

      await images.map(image => uploadImage(image.path));
    });
  }
);

module.exports = app;
